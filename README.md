## Getting Started

First, run the development server:

```bash
yarn
# or
yarn dev



## Project Structure
- **public**: Contains all public files, including assets.
- **src**
  - **components**: Contains all reusable components (layouts, buttons, navbar, etc).
  - **helpers**: Contains helper functions (`pembantu`).
  - **hooks**: Contains custom hooks.
  - **modules**: Contains all views (`modul-modul`).
    - **[Page name]**: Each page's directory.
- **app**: Contains all the pages (`app`).


```
