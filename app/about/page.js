import React from "react";

export default function Page() {
  return (
    <div className="w-screen h-screen flex bg-white items-center justify-center">
      <h1 className="text-black">About Page</h1>
    </div>
  );
}
