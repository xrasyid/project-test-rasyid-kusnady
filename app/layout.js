import { Inter } from "next/font/google";
import "./globals.css";
import config from "@config/config.json";
import Header from "components/Header";
const inter = Inter({ subsets: ["latin"] });

const metadata = config.metadata;

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content={metadata.meta_description} />
        <meta name="keywords" content={metadata.meta_keywords} />
        <meta name="author" content={metadata.meta_author} />
        <title>{metadata.meta_title}</title>
        <link rel="icon" href="/favicon.ico" />
      </head>
      <body className={inter.className}>
        <Header />
        <main>{children}</main>
      </body>
    </html>
  );
}
