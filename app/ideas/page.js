import ArticleIdeas from "modules/ideas/components/ArticleIdeas";
import HeroIdeas from "modules/ideas/components/HeroIdeas";
import React from "react";

export default function Page() {
  return (
    <div className="relative">
      <HeroIdeas />
      <ArticleIdeas />
    </div>
  );
}
