"use client";
import React, { useState, useEffect } from "react";
import axios from "axios";
import useQueryParams from "hooks/useQueryParams";
import Filters from "components/Filters";
import Card from "components/Card";
import PaginationControls from "components/PaginationControls";

const isClient = typeof window !== "undefined";

function ArticleIdeas() {
  const queryParams = useQueryParams();

  const [ideas, setIdeas] = useState([]);
  const [sortedIdeas, setSortedIdeas] = useState(
    queryParams.get("sort") || "-published_at"
  );
  const [appendedIdeas, setAppendedIdeas] = useState(
    queryParams.get("append") || "medium_image"
  );
  const [pageSize, setpageSize] = useState(
    parseInt(queryParams.get("pageSize")) || 8
  );
  const [page, setPage] = useState(parseInt(queryParams.get("page")) || 1);
  const [totalPages, setTotalPages] = useState(0);
  const [totalItems, setTotalItems] = useState(0);

  useEffect(() => {
    if (isClient) {
      queryParams.set("page", page.toString());
      queryParams.set("pageSize", pageSize.toString());
      queryParams.set("sort", sortedIdeas);
      window.history.pushState({}, "", "?" + queryParams.toString());
    }
  }, [sortedIdeas, appendedIdeas, pageSize, page]);

  useEffect(() => {
    const fetchTotalItems = async () => {
      try {
        const response = await axios.get(
          `https://suitmedia-backend.suitdev.com/api/ideas?page[size]=80`
        );
        setTotalItems(response.data.data.length);
        setTotalPages(Math.ceil(response.data.data.length / pageSize));
      } catch (error) {
        console.error("Error fetching total items:", error);
      }
    };

    fetchTotalItems();
  }, []);

  useEffect(() => {
    const fetchIdeas = async () => {
      try {
        const response = await axios.get(
          `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${page}&page[size]=${pageSize}&append[]=${appendedIdeas}&sort=${sortedIdeas}`
        );
        setIdeas(response.data.data);
        console.log("Ideas fetched:", response.data.data);
      } catch (error) {
        console.error("Error fetching ideas:", error);
      }
    };

    fetchIdeas();
    setTotalPages(Math.ceil(totalItems / pageSize));
  }, [page, pageSize, sortedIdeas]);

  return (
    <div className="flex flex-col w-full relative overflow-x-clip bg-white">
      <div className="container px-[16px] lg:px-0 mx-auto">
        <div className="bg-white h-[25vh] flex w-[140%] absolute top-[-5rem] lg:top-[-5rem] left-[-10%] -rotate-3" />
        <div className="bg-white min-h-screen flex flex-col justify-center items-center text-black p-8 lg:p-16 z-10">
          <div className="flex flex-col lg:flex-row w-full justify-between items-center mb-8">
            <div className="flex items-end py-4 z-30 ">
              <p>
                Showing: {(page - 1) * pageSize + 1} -{" "}
                {Math.min(page * pageSize, totalItems)} of {totalItems}
              </p>
            </div>
            <Filters
              sortedIdeas={sortedIdeas}
              pageSize={pageSize}
              onSortedIdeasChange={setSortedIdeas}
              onpageSizeChange={(size) => {
                setpageSize(size);
                setPage(1);
              }}
            />
          </div>
          <Card ideas={ideas} />
          <PaginationControls
            page={page}
            totalPages={totalPages}
            onPageChange={setPage}
          />
        </div>
      </div>
    </div>
  );
}

export default ArticleIdeas;
