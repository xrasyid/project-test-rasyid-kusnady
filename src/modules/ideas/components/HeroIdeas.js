import React from "react";

function HeroIdeas() {
  const backgroundImage = {
    backgroundImage: "url('/assets/ideas/HeroIdeas.jpeg')",
    backgroundSize: "cover",
    backgroundPosition: "top",
  };

  return (
    <div className="flex w-full min-h-screen relative">
      <div
        style={backgroundImage}
        className="flex w-full flex-col min-h-screen  p-4 md:p-6 xl:p-8 bg-fixed bg-black opacity-50"
      ></div>

      <div className="flex w-full flex-col min-h-screen absolute z-10 items-center justify-center">
        <h1 className="text-white text-xl md:text-4xl xl:text-5xl text-center">
          Ideas
        </h1>
        <p className="text-white text-base md:text-lg xl:text-xl text-center">
          Where all our great things begin
        </p>
      </div>
    </div>
  );
}

export default HeroIdeas;
