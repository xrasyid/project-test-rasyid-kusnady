const useQueryParams = () => {
  return typeof window !== "undefined"
    ? new URLSearchParams(window.location.search)
    : new URLSearchParams();
};

export default useQueryParams;
