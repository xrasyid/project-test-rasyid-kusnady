import React from "react";

const Filters = ({
  sortedIdeas,
  pageSize,
  onpageSizeChange,
  onSortedIdeasChange,
}) => {
  return (
    <div className="flex flex-row gap-4 items-center z-30">
      <div className="flex flex-row gap-4 items-center">
        <p>Show per page: </p>
        <select
          className="p-2 border border-gray-300 rounded-lg"
          value={pageSize} // Corrected prop name
          onChange={(e) => onpageSizeChange(parseInt(e.target.value))}
        >
          <option value={8}>8</option>
          <option value={16}>16</option>
          <option value={24}>24</option>
        </select>
      </div>
      <div className="flex flex-row gap-4 items-center ml-1">
        <p>Sort by:</p>
        <select
          className="p-2 border border-gray-300 rounded-lg"
          value={sortedIdeas}
          onChange={(e) => onSortedIdeasChange(e.target.value)}
        >
          <option value="-published_at">Newest</option>
          <option value="published_at">Oldest</option>
        </select>
      </div>
    </div>
  );
};

export default Filters;
