import React from "react";

const PaginationControls = ({ page, totalPages, onPageChange }) => {
  const getPageNumbers = () => {
    const pageNumbers = [];
    const maxDisplayedPages = 4;

    if (totalPages <= maxDisplayedPages) {
      for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(i);
      }
    } else {
      if (page <= 2) {
        // Near the beginning
        for (let i = 1; i <= maxDisplayedPages; i++) {
          pageNumbers.push(i);
        }
        pageNumbers.push("...");
        pageNumbers.push(totalPages);
      } else if (page >= totalPages - 1) {
        // Near the end
        pageNumbers.push(1);
        pageNumbers.push("...");
        for (let i = totalPages - maxDisplayedPages + 1; i <= totalPages; i++) {
          pageNumbers.push(i);
        }
      } else {
        // In the middle
        pageNumbers.push(1);
        pageNumbers.push("...");
        for (let i = page - 1; i <= page + 1; i++) {
          pageNumbers.push(i);
        }
        pageNumbers.push("...");
        pageNumbers.push(totalPages);
      }
    }

    return pageNumbers;
  };

  const pageNumbers = getPageNumbers();

  return (
    <div className="flex w-full justify-center items-center mt-6 lg:mt-12">
      <button
        className="p-2 rounded-lg"
        onClick={() => onPageChange(1)}
        disabled={page === 1}
      >
        {"<<"}
      </button>
      <button
        className="p-2 rounded-lg"
        onClick={() => onPageChange(page - 1)}
        disabled={page === 1}
      >
        {"<"}
      </button>
      {pageNumbers.map((pageNumber, index) => (
        <button
          key={index}
          className={`px-2 py-1 lg:px-4 lg:py-2  rounded-lg ${
            page === pageNumber ? "bg-orange-500 text-white" : ""
          } ${pageNumber === "..." ? "cursor-default" : ""}`}
          onClick={() => {
            if (pageNumber !== "...") {
              onPageChange(pageNumber);
            }
          }}
          disabled={pageNumber === "..."}
        >
          {pageNumber}
        </button>
      ))}
      <button
        className="p-2 rounded-lg"
        onClick={() => onPageChange(page + 1)}
        disabled={page === totalPages}
      >
        {">"}
      </button>
      <button
        className="p-2 rounded-lg"
        onClick={() => onPageChange(totalPages)}
        disabled={page === totalPages}
      >
        {">>"}
      </button>
    </div>
  );
};

export default PaginationControls;
