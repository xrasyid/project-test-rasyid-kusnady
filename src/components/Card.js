import React from "react";
import Image from "next/image";

const Card = ({ ideas }) => {
  return (
    <div className="grid grid-cols-2 lg:grid-cols-4 w-full gap-4 lg:gap-12">
      {ideas.map((idea) => (
        <div
          key={idea.id}
          className="flex flex-col w-full relative overflow-clip rounded-lg lg:rounded-xl shadow-xl"
        >
          <Image
            src={"https://placehold.co/600x400/png"}
            width={1000}
            height={1000}
            className="h-auto w-full object-cover hover:scale-105 transition-transform duration-500 ease-in-out"
            alt={idea.title}
          />
          <div className="flex flex-col flex-wrap p-4">
            <p className="text-xs lg:text-sm text-slate-400">
              {new Date(idea.published_at).toDateString()}
            </p>
            <p className="line-clamp-3 text-sm lg:text-base hover:text-orange-suit-500 hover:scale-[101%] transition-color duration-150 text-black font-bold">
              {idea.title}
            </p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Card;
